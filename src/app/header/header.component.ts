import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  selectCSSClass:string;

  constructor() { }

  ngOnInit() {
  }


  title = 'Angular Course!!';

  loadCssClass()
  {
      this.selectCSSClass="styleHeader";
      console.log(this.selectCSSClass);
  }
}
